import React, { PureComponent } from 'react';
import PrivateRoute from './components/PrivateRoute';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { loginRequest, logout } from './actions/authorization';
import Header from './components/Header';
import Organizations from './components/Organizations';
import HomePage from './components/HomePage';

class App extends PureComponent {
  render() {
    const { logout, loginRequest } = this.props;

    const { isAuthorized, isFetching, error } = this.props.authorization;

    return (
      <BrowserRouter>
        <div className="wrapper">
          <Header isAuthorized={isAuthorized} logout={logout} />
          <Switch>
            <Route
              exact
              path="/"
              render={props => (
                <HomePage
                  {...props}
                  loginRequest={loginRequest}
                  isFetching={isFetching}
                  isAuthorized={isAuthorized}
                  error={error}
                />
              )}
            />
            <PrivateRoute
              path="/organizations"
              isAuthorized={isAuthorized}
              component={Organizations}
            />
            <Redirect to="/" />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => ({
  authorization: state.authorization
});

const mapDispatchToProps = {
  loginRequest,
  logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
