import { take, put, call, fork, cancel } from 'redux-saga/effects';
import {
  LOGIN_REQUEST,
  LOGIN_ERROR,
  LOGOUT,
  loginSuccess,
  loginError
} from '../actions/authorization';

function fetchLogin(username, password) {
  return fetch(`https://demo3497398.mockable.io/${username}/${password}`, {
    method: 'GET'
  })
    .then(response => response.json())
    .then(response => ({ response }))
    .catch(error => ({ error }));
}

const setToken = token => {
  localStorage.setItem('isAuthorized', token);
};

function* authorize(username, password) {
  try {
    const token = yield call(fetchLogin, username, password);
    if (token.response.authorized === 'true') {
      yield put(loginSuccess());
      yield call(setToken, 'true');
    } else {
      yield put(loginError('Неправильный логин или пароль'));
    }
  } catch (error) {
    yield put(loginError('Неправильный логин или пароль'));
  }
}

export default function* loginFlow() {
  while (true) {
    const {
      payload: { username, password }
    } = yield take(LOGIN_REQUEST);
    const task = yield fork(authorize, username, password);
    const action = yield take([LOGOUT, LOGIN_ERROR]);
    if (action.type === LOGOUT) {
      yield cancel(task);
    }
  }
}
