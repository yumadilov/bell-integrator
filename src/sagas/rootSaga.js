import loginFlow from './authorization';
import { call, takeEvery, all } from 'redux-saga/effects';

function* rootSaga() {
  yield all([call(loginFlow)]);
}

export default rootSaga;
