import React, { PureComponent } from 'react';

class Organizations extends PureComponent {
  state = {
    inputs: {
      username: '',
      password: ''
    },
    errors: {
      username: null,
      password: null
    }
  };

  usernameInput = React.createRef();
  passwordInput = React.createRef();

  checkInputs = input => {
    const emailRegExp = /^[A-Z0-9._-]+@[A-Z0-9-]+\.[A-Z]{2,4}$/i;
    const passwordRegExp = /[0-9A-Z_-]{8,}/gi;
    const otherSymbolsRegExp = /[^/.,~`'"{}+!@#$%^&*()_a-z0-9-]+/gi;

    const setError = text => {
      this.setState({
        ...this.state,
        errors: {
          ...this.state.errors,
          [input.name]: text
        }
      });
    };

    if (input.value !== '') {
      if (input.name === 'username') {
        if (!emailRegExp.test(input.value)) {
          setError(
            'Email должен быть следующего вида: user@example.com',
            input
          );
        }
        if (otherSymbolsRegExp.test(input.value)) {
          setError(
            'В этом поле не должны быть символы из русского алфавита',
            input
          );
        }
      } else if (input.name === 'password') {
        if (!passwordRegExp.test(input.value)) {
          setError('Пароль должен быть не менее 8 символов', input);
        }
        if (otherSymbolsRegExp.test(input.value)) {
          setError(
            'Пароль должен состоять только из цифр и латинских символов',
            input
          );
        }
      }
    } else {
      setError('Заполните это поле', input);
    }
  };

  handleChange = event => {
    const { value, name } = event.target;

    this.setState({
      ...this.state,
      inputs: {
        ...this.state.inputs,
        [name]: value
      },
      errors: {
        ...this.state.errors,
        [name]: null
      }
    });
  };

  handleBlur = event => {
    const { value, name } = event.target;

    if (value !== '') {
      if (name === 'username') {
        this.checkInputs(this.usernameInput.current);
      } else if (name === 'password') {
        this.checkInputs(this.passwordInput.current);
      }
    }
  };

  handleSubmit = event => {
    event.preventDefault();

    const { loginRequest } = this.props;
    const { username, password } = this.state.inputs;

    this.checkInputs(this.usernameInput.current);
    this.checkInputs(this.passwordInput.current);

    if (
      this.state.errors.username === null &&
      this.state.errors.password === null
    ) {
      loginRequest(username, password);
    }
  };

  render() {
    const { isAuthorized, isFetching, error } = this.props;
    const { errors } = this.state;

    if (isFetching) {
      return <p>Загрузка...</p>;
    }

    if (!isAuthorized) {
      return (
        <div>
          <h1>Homepage content</h1>
          <form onSubmit={this.handleSubmit}>
            <span>Войти</span>
            <input
              ref={this.usernameInput}
              type="email"
              name="username"
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              value={this.state.inputs.username}
              required
              placeholder="Email"
              className={errors.username === null ? null : 'input--error'}
            />
            {this.state.errors.username ? (
              <div>{this.state.errors.username}</div>
            ) : null}
            <input
              ref={this.passwordInput}
              type="password"
              name="password"
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              value={this.state.inputs.password}
              required
              placeholder="Пароль"
              className={errors.password === null ? null : 'input--error'}
            />
            {this.state.errors.password ? (
              <div>{this.state.errors.password}</div>
            ) : null}
            <button type="submit" onClick={this.handleSubmit}>
              Войти
            </button>
            {error !== null ? (
              <div className="authform__error">{error}</div>
            ) : null}
          </form>
        </div>
      );
    } else {
      return <h1>Homepage content</h1>;
    }
  }
}

export default Organizations;
