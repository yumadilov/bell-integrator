import React, { PureComponent, Fragment } from 'react';
import { NavLink } from 'react-router-dom';

class Header extends PureComponent {
  handleClick = () => {
    const { logout } = this.props;
    logout();
  };

  render() {
    const { isAuthorized } = this.props;
    return (
      <div>
        <header>
          <nav>
            <NavLink to="/">
              <span>Logotype</span>
            </NavLink>
            {isAuthorized ? (
              <Fragment>
                <NavLink to="/organizations">Организации</NavLink>
                <button onClick={this.handleClick}>Выйти</button>
              </Fragment>
            ) : (
              <NavLink to="/">Войти</NavLink>
            )}
          </nav>
        </header>
      </div>
    );
  }
}

export default Header;
