import React from 'react';
import { Route, Redirect } from 'react-router-dom';

class PrivateRoute extends React.Component {
  render() {
    const { isAuthorized } = this.props;
    const { component: RouteComponent, ...rest } = this.props;
    return (
      <Route
        render={props =>
          isAuthorized ? (
            <RouteComponent {...props} {...rest} />
          ) : (
            <Redirect to="/" />
          )
        }
      />
    );
  }
}

export default PrivateRoute;
