export const GET_ORGANIZATIONS_REQUEST = 'GET_ORGANIZATIONS_REQUEST';
export const GET_ORGANIZATIONS_SUCCESS = 'GET_ORGANIZATIONS_SUCCESS';
export const GET_ORGANIZATIONS_ERROR = 'GET_ORGANIZATIONS_ERROR';

export const SET_ORGANIZATIONS_REQUEST = 'SET_ORGANIZATIONS_REQUEST';
export const SET_ORGANIZATIONS_SUCCESS = 'SET_ORGANIZATIONS_SUCCESS';
export const SET_ORGANIZATIONS_ERROR = 'SET_ORGANIZATIONS_ERROR';

export const getOrganizationsRequest = () => ({
  type: GET_ORGANIZATIONS_REQUEST
});

export const getOrganizationsSuccess = () => ({
  type: GET_ORGANIZATIONS_SUCCESS
});

export const getOrganizationsError = payload => ({
  type: GET_ORGANIZATIONS_ERROR,
  payload
});

export const setOrganizationsRequest = (id, name, address, inn) => ({
  type: SET_ORGANIZATIONS_REQUEST,
  payload: {
    id,
    name,
    address,
    inn
  }
});

export const setOrganizationsSuccess = () => ({
  type: SET_ORGANIZATIONS_SUCCESS
});

export const setOrganizationsError = payload => ({
  type: SET_ORGANIZATIONS_ERROR,
  payload
});
