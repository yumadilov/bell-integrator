import {
  GET_ORGANIZATIONS_ERROR,
  GET_ORGANIZATIONS_SUCCESS,
  GET_ORGANIZATIONS_REQUEST,
  SET_ORGANIZATIONS_ERROR,
  SET_ORGANIZATIONS_SUCCESS,
  SET_ORGANIZATIONS_REQUEST
} from '../actions/organizations';

const initialState = {
  error: null,
  isFetching: false,
  isFetched: false
};

const organizations = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORGANIZATIONS_REQUEST:
      return {
        ...state,
        isFetching: true,
        isFetched: false
      };
    case GET_ORGANIZATIONS_SUCCESS:
      return {
        ...state,
        isFetched: true,
        isFetching: false,
        error: null
      };
    case GET_ORGANIZATIONS_ERROR:
      return {
        ...state,
        isFetched: true,
        isFetching: false,
        error: action.payload
      };
    case SET_ORGANIZATIONS_REQUEST:
      return {};
    case SET_ORGANIZATIONS_SUCCESS:
      return {};
    case SET_ORGANIZATIONS_ERROR:
      return {};
    default:
      return state;
  }
};

export default organizations;
